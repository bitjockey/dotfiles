export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"

source $HOME/.poetry/env

autoload colors
colors

### Added by Zinit's installer
if [[ ! -f $HOME/.zinit/bin/zinit.zsh ]]; then
    print -P "%F{33}▓▒░ %F{220}Installing DHARMA Initiative Plugin Manager (zdharma/zinit)…%f"
    command mkdir -p "$HOME/.zinit" && command chmod g-rwX "$HOME/.zinit"
    command git clone https://github.com/zdharma/zinit "$HOME/.zinit/bin" && \
        print -P "%F{33}▓▒░ %F{34}Installation successful.%f" || \
        print -P "%F{160}▓▒░ The clone has failed.%f"
fi
source "$HOME/.zinit/bin/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit
### End of Zinit installer's chunk
# A.
setopt promptsubst
setopt prompt_subst

# B.
zinit ice wait lucid
zinit snippet OMZ::lib/git.zsh

# C.
zinit ice wait atload"unalias grv" lucid
zinit snippet OMZ::plugins/git/git.plugin.zsh

# D.
PS1="READY >" # provide a nice prompt till the theme loads
zinit ice wait'!' lucid
zinit light denysdovhan/spaceship-prompt

# E.
zinit ice wait lucid
zinit snippet OMZ::plugins/colored-man-pages/colored-man-pages.plugin.zsh

# F.
zinit ice wait as"completion" lucid
zinit snippet OMZ::plugins/docker/_docker

# G.
zinit ice wait atinit"zpcompinit" lucid
zinit light zdharma/fast-syntax-highlighting

# H.
zinit ice atclone"dircolors -b LS_COLORS > clrs.zsh" \
    atpull'%atclone' pick"clrs.zsh" nocompile'!' \
    atload'zstyle ":completion:*" list-colors “${(s.:.)LS_COLORS}”'
zinit light trapd00r/LS_COLORS

### Aliases
alias ls='ls --color'


