# My Dotfiles

```bash
git clone <path to this> $HOME/dotfiles
```

## `stow`

### Installation

```bash
sudo apt update && sudo apt install -y stow
```

## `zsh`

* [zinit](https://github.com/zdharma/zinit) - fast zsh plugin manager

### Installation

```bash
# Stow
cd $HOME/dotfiles
stow zsh

# Install zsh in Ubuntu/Pop!_OS
sudo apt update && sudo apt install -y zsh
chsh -s $(which zsh)

# Start zsh and install zinit
zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/zdharma/zinit/master/doc/install.sh)"
```
